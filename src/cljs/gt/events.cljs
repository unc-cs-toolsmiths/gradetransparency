(ns gt.events
  "This is where k/reg-event-fx and k/reg-event/db calls go to register the
  re-frame effects for events."
  (:require [kee-frame.core :as k]
            [re-frame.core :as rf :refer [path]]))

(k/reg-event-fx :navigate-to
  (fn [cofx args]
    {:navigate-to args}))

;; Example:
;; (k/reg-event-db :stop-editing-field-for-college
;;   (fn [db [college-id]]
;;     (update-in db
;;                [:page :page-type/student :editing-college-list-entry-field]
;;                dissoc college-id)))
