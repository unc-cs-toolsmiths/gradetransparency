(ns system
  "Start a 'system' of related (and maybe stateful, running) components.

  This namespace is only responsible for dev and test environments. The
  production environment will be handled separately, later."

  (:require
   [clojure.spec.alpha :as s]
   [server :as server]
   [integrant.core :as ig]))

;;; Specify required arguments to system components and their types with
;;; `ig/pre-init-spec`:

(s/def ::handler fn?)
(s/def ::port number?)

(defmethod ig/pre-init-spec :server/http [_]
  (s/keys :req-un [::handler]
          :opt-un [::port]))

;;; Provide default arguments for system components with `ig/prep-key` (which
;;; requires calling `ig/prep` before `ig/init`):

(defmethod ig/prep-key :server/http [_ config]
  (merge {:port 3000} config))

;;; Start system components using `ig/init-key`:

(defmethod ig/init-key :server/http [_ config]
  (server/start-server config))

;;; Halt system components, if needed, using `ig/halt-key!`:

(defmethod ig/halt-key! :server/http [_ server]
  (server/stop-server server))

(defn start-system
  "Given a system config, start the system."
  [system-config]
  (let [port (-> system-config :server/http :port)
        system (-> system-config ig/prep ig/init)]
    (println "System started.")
    (when port
      (println "HTTP server listening on port" port))
    system))

(defn stop-system
  "Stop a running system returned from `start-system`."
  [system]
  (ig/halt! system))
