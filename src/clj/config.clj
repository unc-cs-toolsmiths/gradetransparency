(ns config
  (:require [routes :refer [base-app]]
            [config.core :refer [env]]
            [integrant.core :as ig]))

(def dev-system-config
  {:server/http {:allowed-origin "http://localhost:3000"
                 :handler base-app
                 :port (:port env 3000)}})
