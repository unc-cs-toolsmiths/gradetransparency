(ns gt.page.role-choice
  (:require [kee-frame.core :as k]))

(defn role-choice-page []
  [:div {:id "home"}
   [:p>a {:href (k/path-for [:prof-login])} "Hello I'm a Professor"]
   [:p>a {:href (k/path-for [:stud-login])} "Hello I'm a Student"]])
