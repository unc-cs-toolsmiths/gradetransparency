(ns middleware)

(defn wrap-catch [handler]
  (fn [req]
    (try
      (handler req)
      (catch Throwable e
        {:status 500
         :headers {"content-type" "text/plain"}
         :body (pr-str {:error e, :request req})}))))
