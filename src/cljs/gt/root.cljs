(ns gt.root
  (:require
  [gt.page.role-choice :refer [role-choice-page]]
   [gt.page.profLogin :refer [prof-login-page]]
   [gt.page.profView :refer [render-logged-proffesor]]
   [gt.page.studLogin :refer [stud-login-page]]
   [kee-frame.core :as k]))

(def routes
  [["/" :role-choice]
   ["/prof" :prof-unauthenticated]
   ["/profLogin" :prof-login]
   ["/prof/View" :prof-view]
   ["/studLogin" :stud-login]
   ["/stud/View" :stud-view]])

(k/reg-controller :role-choice
  {:params #(when (= :role-choice (-> % :data :name)) true)
   :start (fn [] nil)})

(k/reg-controller :prof-unauthenticated
  {:params #(when (= :prof-unauthenticated (-> % :data :name)) true)
   :start (fn [] nil)})

(k/reg-controller :prof-login
  {:params #(when (= :prof-login (-> % :data :name)) true)
   :start (fn [] nil)})

(k/reg-controller :prof-view
  {:params #(when (= :prof-view (-> % :data :name)) true)
   :start (fn [] nil)})

(k/reg-controller :stud-login
  {:params #(when (= :stud-login (-> % :data :name)) true)
   :start (fn [] nil)})

(defn root-component []
  (k/switch-route (fn [route] (-> route :data :name))
    :role-choice [role-choice-page]
    :prof-login [prof-login-page]
    :stud-login [stud-login-page]
    :prof-view [render-logged-proffesor]
    nil [role-choice-page]))
