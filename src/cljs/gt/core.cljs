(ns ^:figwheel-hooks gt.core
  (:require
   [gt.config :as config]
   [gt.db-spec :as db-spec]
   [gt.events :as events]
   [gt.root :as root]
   [kee-frame.core :as k]
   kee-frame.scroll
   [re-frame.core :as rf]))

(defn dev-setup []
  (when config/DEBUG
    (enable-console-print!)
    (println "dev mode active")))

(defn start-app []
  (rf/clear-subscription-cache!)
  (k/start! {:routes root/routes
             :app-db-spec ::db-spec/db-spec
             :initial-db {}
             :root-component [root/root-component]
             :debug? config/DEBUG}))

(defn ^:after-load re-render []
  (start-app))

(defn ^:export init []
  (dev-setup)
  (start-app))
