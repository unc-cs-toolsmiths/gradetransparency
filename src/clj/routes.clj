(ns routes
  (:require
   [clojure.java.io :as io]
   [handler.index :as index]
   (reitit
     [core :as r]
     [ring :as ring])))

(defn echo-handler [req]
  {:status 200
   :headers {"content-type" "application/edn"}
   :body (pr-str req)})

(def router
  (ring/router
    [["/" {:get index/handler, :name ::index}]
     ["/api"
      ["/echo" {:get echo-handler, :name ::example}]]]))

(defn default-handler [req]
  (let [{:keys [uri]} req
        rel-path (str "public" uri)
        resource (io/resource rel-path)]
    (if resource
      {:status 200
       :body (io/input-stream resource)}
      (index/handler req))))

(def base-app (ring/ring-handler router default-handler))
