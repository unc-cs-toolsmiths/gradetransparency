(ns gt.page.profLogin
  (:require
   [re-frame.core :as rf]
   [reagent.core :as r]))

(rf/reg-sub :login-subpage
  (fn [app-db _]
    (:login-subpage app-db)))

(rf/reg-event-db :set-form-subpage
  (fn [db [_ new-subpage]]
    ;;(js/console.log new-subpage)
    (assoc db :login-subpage new-subpage)))

(def app-state
  (r/atom
   {:correctuser "testprofessor"
    :correctpwd "123"}))

(defn validate-Signup[email, emailConfirm, pass, pass confirm ] 

)

;; Form to be rendered
(defn login-form []
  [:form#loginForm
   [:input {:id "emailLogin" :placeholder "Enter Email"}]
   [:input {:id "pwLogin" :placeholder "Enter Password"}]
   [:button {:id "submitLogin" :onClick (fn [event] 
                                          (.preventDefault event)
                                          (when (and (== (.-value (js/document.getElementById "emailLogin")) (:correctuser @app-state))
                                                     (== (.-value (js/document.getElementById "pwLogin")) (:correctpwd @app-state)))
                                            (rf/dispatch [:navigate-to :prof-view])))} "Enter"]])

(defn signup-form []
  [:form#signUpForm
   [:input {:id "emailsignUp" :placeholder "Enter Email"}]
   [:br]
   [:input {:id "emailconfirm" :placeholder "Confrim Email"}]
   [:br]
   [:input {:id "pwsignUp" :placeholder "Create Password"}]
   [:br]
   [:input {:id "pwconfirm" :placeholder "Confrim Password"}]
   [:button {:id "submitSignup"} "Enter"]])

;; Select form on open
(defn prof-login-page []
  [:div {:id "wrappingDiv" :style/indent "center"}
   [:div {:id "btnDiv"}
    [:button {:id "loginBtn" :on-click #(rf/dispatch [:set-form-subpage :login])} "LOGIN"]
    [:button {:id "signUp" :on-click #(rf/dispatch [:set-form-subpage :signup])} "SignUp"]]
   [:div {:id "formDiv"}
    (case @(rf/subscribe [:login-subpage])
      :login [login-form]
      :signup [signup-form]
      nil)]])