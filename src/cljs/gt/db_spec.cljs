(ns gt.db-spec
  "Kee-frame has a very nice feature where, before resetting the app-db to a new
  value, it checks whether the new value conforms to a spec. If so, it proceeds
  with the reset. If not, it refuses to reset the value and prints an error
  message to the console. This identifies likely bugs as soon as possible, when
  the re-frame effects are actually being handled. This namespace defines that
  spec; the bottom `db-spec` def is what the app-db values are actually checked
  against."
  (:require [cljs.spec.alpha :as s]))

(s/def ::counter number?)

(s/def ::db-spec
  (s/keys :opt-un
          [::counter]))
