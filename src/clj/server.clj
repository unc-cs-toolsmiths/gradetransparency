(ns server
  (:require
    [middleware :as mdw]
    [ring.adapter.jetty :as jetty]
    [ring.middleware.defaults :refer [wrap-defaults site-defaults]]))

(defn make-handler [base-handler]
  (let [defaults-cfg (assoc-in site-defaults [:security :anti-forgery] false)]
    (-> base-handler
        (wrap-defaults defaults-cfg)
        mdw/wrap-catch)))

(defn start-server
  [{:keys [handler port] :as config}]
  (let [wrapped-handler (make-handler handler)]
    (jetty/run-jetty wrapped-handler {:port port, :join? false})))

(defn stop-server [server]
  (.stop server))
