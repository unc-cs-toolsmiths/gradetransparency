# Grade Transparency

Grade Transparency is re-fram application to provide instructors the ability to 
return grades to their students in a more timely manner.

## Development Mode

The application in development mode needs two processes running: a backend
server and a Figwheel process to compile Clojurescript (CLJS) to JS. First,
one small setup step: mkdir /tmp/premiumprep-files/, or whatever you've set as
the :storage-path configuration (see the configuration section below). Then,
start the backend server on localhost:3000:

```clojure -A:backend```

Then start the Figwheel process. Once the CLJS is compiled, it will open up a
web page with the app, with hot-reloaded enabled. This means that when you make
changes to the CLJS source, they will automatically propagate to the app running
in the browser tab. Look for the Figwheel icon to pop up briefly in the
lower-left corner of the page. It will be green if your changes compiled
successfully.

```clojure -A:fig:frontend-dev```

You can also use entr to restart the backend
server whenever code changes like so:

find src/clj -type f -name \*.clj | entr -r clojure -A:backend

## Running Tests

## Deployment Process
s

## License