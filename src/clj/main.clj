(ns main
  (:require [config :as config]
            [system :as system])
  (:gen-class))

(defn -main [& args]
  (system/start-system config/dev-system-config))
