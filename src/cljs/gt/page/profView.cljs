(ns gt.page.profView
(:require
 [re-frame.core :as rf]
 [reagent.core :as r]))

(rf/reg-sub :entry-subpage
(fn [app-db _]
  (:entry-subpage app-db)))

(rf/reg-event-db :set-entry-form
(fn [db [_ new-subpage]]
  ;;(js/console.log new-subpage)
  (assoc db :entry-subpage new-subpage)))

(defn add-course-form []
  [:div
   [:input {:placeholder "Course Dept"}]
   [:input  {:placeholder "Course Number"}]
   [:input  {:placeholder "Academic Term"}]
   [:input  {:placeholder "Academic Year"}]
   [:<> [:button "Cancel"] [:button "Add"]]])

(defn add-grading-cat []
[:div {:id "addCatForm"}
 [:h4 "Enter Catergory Name"] [:input {:placeholder "Category Name"}]
 [:h4 "Enter Category Weight as Percentage"]
 [:input  {:placeholder "Weight as Percentage"}]"%"
 [:<> [:button "Cancel"] [:button "Add"]]])


;; left side panel to show course for the logged in professor
(defn course-list []
  [:div.column {:id "courseList"}
   ;; should loop for all courses a professor has
   [:div {:id "sub-courseList"}
    [:p "No Courses Listed (FOR NOW)"]
    [:button {:on-click #(rf/dispatch [:set-entry-form :add-course])} "Add Class"]]
   [:div {:id "formDiv"}
    (case @(rf/subscribe [:entry-subpage])
      :add-course [add-course-form]
      nil)]])

;; right side of screen
(defn cats-list []
  [:div.rightCol {:id "categoriesDiv"}
   ;; eventually should add loop to loop through CLICKED course in Left Column
   [:h1 "Grading Categories"]
   [:p "You don't have any grading categories yet."]
   [:button {:on-click #(rf/dispatch [:set-entry-form :add-category])} "Add Category"]
   [:div {:id "formDiv"}
    (case @(rf/subscribe [:entry-subpage])
      :add-category [add-grading-cat]
      nil)]])




(defn grading-categories []
[:<> [course-list] [cats-list]])

;; Function that is called when professor logins in 
(defn render-logged-proffesor [professor]
  [grading-categories])