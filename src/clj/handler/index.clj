(ns handler.index
  (:require [clojure.java.io :as io]
            [clojure.string :as str]))

(defn handler [_req]
  (println "Calling index handler")
  {:status 200
   :headers {"content-type" "text/html"}
   :body (-> "public/index.html" io/resource slurp)})
