(ns gt.config)

(goog-define DEBUG false)
(goog-define API_HOST "")

(js/console.log "api-host" API_HOST)

(defn api-url [path]
  (str API_HOST "/api" path))
