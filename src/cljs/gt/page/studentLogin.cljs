(ns gt.page.studLogin
  (:require
   [re-frame.core :as rf]
   [reagent.core :as r]
   [kee-frame.core :as k]))

(rf/reg-sub :login-subpage
  (fn [app-db _]
    (:login-subpage app-db)))

(rf/reg-event-db :set-form-subpage
  (fn [db [_ new-subpage]]
    ;;(js/console.log new-subpage)
    (assoc db :login-subpage new-subpage)))

;; validators of submit
(defn validate-Login[user, pass]
 (if ( (= user  "teststudent") (= pass "123") ) 
     :studView ))

(defn validate-Signup[email, emailConfirm, pass, pass confirm ] 

)

;; Form to be rendered
(defn login-form []
  [:form#loginForm
   [:input {:id "emailLogin" :placeholder "Enter Email"}]])

;; Select form on open
(defn stud-login-page []
  [:div {:id "wrappingDiv" :style/indent "center"}
   [:form 
    [:input {:id "studentID" :placeholder "Enter Student ID"}]
    [:p>a {:href (k/path-for [:stud-view])} "Submit"]]])